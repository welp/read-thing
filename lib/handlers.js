module.exports = {
  login: require('./handlers/login.js'),
  oauth: require('./handlers/oauth.js'),
  root: require('./handlers/root.js'),
  upload: require('./handlers/upload.js')
}
