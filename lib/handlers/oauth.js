const OAuth = require('oauth').OAuth;
const o_config = require('../../config/oauth.js');
const oa = new OAuth(
  o_config.request_url,
  o_config.access_url,
  o_config.client_key,
  o_config.client_secret,
  o_config.version,
  o_config.callback_url,
  o_config.method
);
const log = require('../log.js')

module.exports = function (req, res) {
  const qt = req.query.oauth_token;
  const qv = req.query.oauth_verifier;

  // Twitter doesn't use the oauth_token_secret
  // http://stackoverflow.com/a/9857158
  oa.getOAuthAccessToken(qt, undefined, qv, (err, at, as, result) => {
    if (err) {
      log.error(err)
      return res.sendStatus(500)
    }

    return oa.get('https://api.twitter.com/1.1/account/verify_credentials.json', at, as, (err, data, result) => {
      if (err) {
        log.error(err)
        return res.sendStatus(500)
      }

      req.cookies.set(
        'twitter_id',
        JSON.parse(data).id_str,
        { signed: true, secure: (typeof(PhusionPassenger) !== 'undefined') }
      )

      return res.redirect('/')
    });
  });
}
